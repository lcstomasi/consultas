<?php
/**
 * Consulta Active Record
 * @author  <your-name-here>
 */
class Consulta extends TRecord
{
    const TABLENAME = 'consulta';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    
    
    private $consultorio;
    private $estado_consulta;
    private $system_user;
    private $system_name;
    private $paciente;
    private $medicamentos;

    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('dt_consulta');
        parent::addAttribute('turno');
        parent::addAttribute('diagnostico');
        parent::addAttribute('consultorio_id');
        parent::addAttribute('estado_consulta_id');
        parent::addAttribute('system_user_id');
        parent::addAttribute('system_user_name');
        parent::addAttribute('paciente_id');
    }

    
    /**
     * Method set_consultorio
     * Sample of usage: $consulta->consultorio = $object;
     * @param $object Instance of Consultorio
     */
    public function set_consultorio(Consultorio $object)
    {
        $this->consultorio = $object;
        $this->consultorio_id = $object->id;
    }
    
    /**
     * Method get_consultorio
     * Sample of usage: $consulta->consultorio->attribute;
     * @returns Consultorio instance
     */
    public function get_consultorio()
    {
        // loads the associated object
        if (empty($this->consultorio))
            $this->consultorio = new Consultorio($this->consultorio_id);
    
        // returns the associated object
        return $this->consultorio;
    }
    
    
    /**
     * Method set_estado_consulta
     * Sample of usage: $consulta->estado_consulta = $object;
     * @param $object Instance of EstadoConsulta
     */
    public function set_estado_consulta(EstadoConsulta $object)
    {
        $this->estado_consulta = $object;
        $this->estado_consulta_id = $object->id;
    }
    
    /**
     * Method get_estado_consulta
     * Sample of usage: $consulta->estado_consulta->attribute;
     * @returns EstadoConsulta instance
     */
    public function get_estado_consulta()
    {
        // loads the associated object
        if (empty($this->estado_consulta))
            $this->estado_consulta = new EstadoConsulta($this->estado_consulta_id);
    
        // returns the associated object
        return $this->estado_consulta;
    }
    
    public function get_consultorio_nome()
    {
        if( empty($this->consultorio) )
        {
            $this->consultorio = new Consultorio($this->consultorio_id);
        }
        return $this->consultorio->nome;
    }
    
    
    /**
     * Method set_system_user
     * Sample of usage: $consulta->system_user = $object;
     * @param $object Instance of SystemUser
     */
    public function set_system_user(SystemUser $object)
    {
        $this->system_user = $object;
        $this->system_user_id = $object->id;
    }
    
    /**
     * Method get_system_user
     * Sample of usage: $consulta->system_user->attribute;
     * @returns SystemUser instance
     */
    public function get_system_user()
    {
        // loads the associated object
        if (empty($this->system_user))
            $this->system_user = new SystemUser($this->system_user_id);
    
        // returns the associated object
        return $this->system_user;
    }
    
    
    /**
     * Method set_paciente
     * Sample of usage: $consulta->paciente = $object;
     * @param $object Instance of Paciente
     */
    public function set_paciente(Paciente $object)
    {
        $this->paciente = $object;
        $this->paciente_id = $object->id;
    }
    
    /**
     * Method get_paciente
     * Sample of usage: $consulta->paciente->attribute;
     * @returns Paciente instance
     */
    public function get_paciente()
    {
        // loads the associated object
        if (empty($this->paciente))
            $this->paciente = new Paciente($this->paciente_id);
    
        // returns the associated object
        return $this->paciente;
    }
    
    public function get_system_user_nome()
    {
        TTransaction::open('permission');
        $nome = $this->get_system_user()->name;
        TTransaction::close();
        return $nome;
    }
    
    /**
     * Method addMedicamento
     * Add a Medicamento to the Consulta
     * @param $object Instance of Medicamento
     */
    public function addMedicamento(Medicamento $object)
    {
        $this->medicamentos[] = $object;
    }
    
    /**
     * Method getMedicamentos
     * Return the Consulta' Medicamento's
     * @return Collection of Medicamento
     */
    public function getMedicamentos()
    {
        return $this->medicamentos;
    }

    
    /**
     * Method getConsultorios
     */
    public function getConsultorios()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('consulta_id', '=', $this->id));
        return Consultorio::getObjects( $criteria );
    }
    

    /**
     * Reset aggregates
     */
    public function clearParts()
    {
        $this->medicamentos = array();
    }

    /**
     * Load the object and its aggregates
     * @param $id object ID
     */
    public function load($id)
    {
        $this->medicamentos = parent::loadAggregate('Medicamento', 'ConsultaMedicamento', 'consulta_id', 'medicamento_id', $id);
    
        // load the object itself
        return parent::load($id);
    }

    /**
     * Store the object and its aggregates
     */
    public function store()
    {
        // store the object itself
        parent::store();
    
        parent::saveAggregate('ConsultaMedicamento', 'consulta_id', 'medicamento_id', $this->id, $this->medicamentos);
    }

    /**
     * Delete the object and its aggregates
     * @param $id object ID
     */
    public function delete($id = NULL)
    {
        $id = isset($id) ? $id : $this->id;
        parent::deleteComposite('ConsultaMedicamento', 'consulta_id', $id);
    
        // delete the object itself
        parent::delete($id);
    }


}
