<?php
/**
 * Paciente Active Record
 * @author  <your-name-here>
 */
class Paciente extends TRecord
{
    const TABLENAME = 'paciente';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('nome');
        parent::addAttribute('telefone');
    }

    
    /**
     * Method getConsultas
     */
    public function getConsultas()
    {
        $criteria = new TCriteria;
        $criteria->add(new TFilter('paciente_id', '=', $this->id));
        return Consulta::getObjects( $criteria );
    }
    


}
