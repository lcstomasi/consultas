<?php
/**
 * Avisos Active Record
 * @author  <your-name-here>
 */
class Avisos extends TRecord
{
    const TABLENAME = 'avisos';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'serial'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('nome');
        parent::addAttribute('mensagem');
        parent::addAttribute('dt_aviso');
        parent::addAttribute('estado');
    }
    
    public static function getAvisos()
    {
        $avisos = array();
        
        $html = new THtmlRenderer('app/templates/sino.html');
               
        
        try
        {
            TTransaction::open('db_consultas');                       
            
            $repos = new TRepository('Avisos');
            
            $criterio = new TCriteria();
            $criterio->add( new TFilter( 'estado' , '=' , 'A' ) );
            
            $repos = $repos->load( $criterio );
              
                
            foreach ( $repos as $row )
            {                
                $avisos[] = array( 'id' => $row->id ,'nome' => $row->nome , 'mensagem' => $row->mensagem , 'data' => $row->dt_aviso ); 
            }             
            TTransaction::close();         
                       
            $count = count($avisos);
            
            $count = ($count != 0)? $count : '' ;
            
            $html->enableSection('main' , array('count' => $count) );
            $html->enableSection( 'mensagem' , $avisos , true );
            
                          
        }
        catch ( Exception $e )
        {
            new TMessage( 'error' , $e->getMessage() );
        } 
        
        return($html->getContents() );
    }

}
