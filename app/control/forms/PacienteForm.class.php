<?php
/**
 * PacienteForm Registration
 * @author  <your name here>
 */
class PacienteForm extends TStandardForm
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        
        parent::setDatabase('db_consultas');              // defines the database
        parent::setActiveRecord('Paciente');     // defines the active record
        
        
        // creates the form
        $this->form = new TQuickForm('form_Paciente');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 100%';
        
        
        // define the form title
        $this->form->setFormTitle('Paciente');      
            
            
        // create the form fields
        $id                             = new TEntry('id');
        $nome                           = new TEntry('nome');
        $telefone                       = new TEntry('telefone');


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Nome', $nome,  500, new TRequiredValidator );
        $this->form->addQuickField('Telefone', $telefone,  500, new TRequiredValidator );
        
        
        // editable    
        $id->setEditable( FALSE );
        
        
        // mask
        $telefone->setMask("(99)9999-9999");
        
       
        
        // create the form actions
        $this->form->addQuickAction(_t('Save'), new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array($this, 'onEdit')), 'ico_new.png');
        
        
        // add the form to the page
        parent::add($this->form);
    }
}
