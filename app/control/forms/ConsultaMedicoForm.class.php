<?php
/**
 * ConsultaForm Registration
 * @author  <your name here>
 */
class ConsultaMedicoForm extends TStandardForm 
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');              // defines the database
        parent::setActiveRecord('Consulta');     // defines the active record
        
        // creates the form
        $this->form = new TQuickForm('form_Consulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 600px';
        
        // define the form title
        $this->form->setFormTitle('Consulta');
        
        // create the form fields
        $id                             = new TEntry('id');
        $dt_consulta                    = new TEntry('dt_consulta');
        $paciente_id                    = new TEntry('paciente_id');
        $diagnostico                    = new TText('diagnostico');
            

        // add the fields
        $this->form->addQuickField('ID',            $id,           400);
        $this->form->addQuickField('Data Consulta', $dt_consulta,  400);
        $this->form->addQuickField('Paciente',      $paciente_id,  400);
        $this->form->addQuickField('Diagnóstico',   $diagnostico,  400, new TRequiredValidator);

        
        $diagnostico->setSize(400, 100);
        
        //medicamentos MULTIFIELD
        $multifield = new TMultiField('medicamentos');         
        $med_id     = new TDBSeekButton('med_id', 'db_consultas' , 'form_Consulta' ,'Medicamento', 'nome' , 'medicamentos_id', 'medicamentos_nome' ); 
        $med_nome   = new TEntry('med_nome');    
        
        $multifield->addField('id'  , 'Medicamento' , $med_id   , 100 );
        $multifield->addField('nome', '' , $med_nome ,  300 );
        $multifield->setHeight(200);
        $multifield->setOrientation('horizontal');
        $this->form->addQuickField( '' , $multifield , 200);

        // setEditable
        $id->setEditable( FALSE );
        $paciente_id->setEditable( FALSE );
        $dt_consulta->setEditable( FALSE );
        $med_nome->setEditable( FALSE );
        
        // create the form actions
        $this->form->addQuickAction(_t('Save'), new TAction(array($this, 'onSave')), 'ico_save.png');
        $this->form->addQuickAction(_t('List'),  new TAction(array('ConsultasMedicoList', 'onReload')), 'ico_datagrid.png');
        
        // add the form to the page
        parent::add($this->form);
    }
    
    public function onSave()
    {   
       $dados = $this->form->getData( 'Consulta' );
       
       try
       {
           TTransaction::open('db_consultas');
           
           $consulta = new Consulta( $dados->id );
           
           $consulta->diagnostico = $dados->diagnostico;
           
           $consulta->estado_consulta_id = 3;
           
           $medicamentos = $dados->medicamentos;
           
                     
           $rep = new TRepository('ConsultaMedicamento');
           
           $criterio = new TCriteria();
           
           $criterio->add( new TFilter( 'consulta_id' , '=' , $dados->id ) ); 
           
           $rep = $rep->load($criterio);
           
           foreach ($medicamentos as $medicamento )
           {
               $med = new Medicamento($medicamento->id);
               
               $medExistente = false;
               
               foreach ( $rep as $row )
               {
                   if ($row->medicamento_id == $med->id )
                   {
                       $medExistente = true;
                   }
               }
               
               if( !$medExistente )
               {
                   $consulta->addMedicamento( $med );
               }
           }
                       
           $consulta->store();
           
           TTransaction::close();    
           
           new TMessage( 'info' , 'Consulta Finalizada!' );
        }
        catch (Exception $e )
        {
            new TMessage( 'error' , 'Erro ao Finalizar Consulta!' );
        }     
    }
    
    public function onEdit($param)
    {
        parent::onEdit($param);
         
        $multi = $this->form->getField( 'medicamentos' );
        
        try
        {
            TTransaction::open( 'db_consultas' );             
            $cons = new Consulta($param['key']);
            $multi->setValue($cons->getMedicamentos() );
            
            TTransaction::close();
        }
        catch (Exception $e)
        {
            new TMessage( 'error' , 'falha' );
        }
    
    }
}
