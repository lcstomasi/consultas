<?php
/**
 * ConsultaForm Registration
 * @author  <your name here>
 */
class ConsultaForm extends TPage
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        // creates the form
        $this->form = new TForm('form_Consulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 100%';
        
        // add a table inside form
        $table = new TTable;
        $table-> width = '100%';
        $this->form->add($table);
        
        // add a row for the form title
        $row = $table->addRow();
        $row->class = 'tformtitle'; // CSS class
        $row->addCell( new TLabel('Consulta') )->colspan = 2;
        
        $criteria = new TCriteria();
        $criteria->add(new TFilter('id', 'in', 'NOESC:(SELECT system_user_id FROM system_user_group where system_group_id = 3)'));

        // create the form fields
        $id                             = new TEntry('id');
        $dt_consulta                    = new TDate('dt_consulta');
        $turno                          = new TRadioGroup('turno');   
        $consultorio_id                 = new TDBSeekButton('consultorio_id' , 'db_consultas' , 'form_Consulta' , 'Consultorio' , 'sala' , 'consultorio_id', 'consultorio_nome' );
        $consultorio_nome               = new TEntry('consultorio_nome');
        $estado_consulta_id             = new TDBCombo('estado_consulta_id' , 'db_consultas' , 'EstadoConsulta' , 'id' , 'descricao' );
        $system_user_id                 = new TDBSeekButton('system_user_id','permission','form_Consulta','SystemUser','name', 'system_user_id' ,'system_user_name', $criteria );
        $system_user_name               = new TEntry('system_user_name'); 
        $paciente_id                    = new TDBSeekButton('paciente_id' , 'db_consultas' , 'form_Consulta' , 'Paciente' , 'nome' ,'paciente_id' , 'paciente_nome' );
        $paciente_nome                  = new TEntry('paciente_nome'); 
        
        $turno->addItems(array( 'M' => 'Manhã' , 'T' => 'Tarde' ) );   
        $turno->setLayout('horizontal');

        $turno->setValue('M');

       
        // define the sizes
        $id->setSize(520);
        $dt_consulta->setSize(500);
        $consultorio_id->setSize(50);
        $estado_consulta_id->setSize(500);
        $system_user_id->setSize(50);
        $paciente_id->setSize(50);
        $consultorio_nome->setSize(450);
        $estado_consulta_id->setSize(520);
        $paciente_nome->setSize(450);
        $system_user_name->setSize(450);
    
        // editable 
        $id->setEditable( FALSE );
        $consultorio_nome->setEditable( FALSE );
        $paciente_nome->setEditable( FALSE );
        $system_user_name->setEditable( FALSE ); 
        
        // validations
        $dt_consulta->addValidation('dt_consulta', new TRequiredValidator);
        $consultorio_id->addValidation('consultorio_id', new TRequiredValidator);
        $estado_consulta_id->addValidation('estado_consulta_id', new TRequiredValidator);
        $system_user_id->addValidation('system_user_id', new TRequiredValidator);
        $system_user_name->addValidation('system_user_name', new TRequiredValidator);
        $paciente_id->addValidation('paciente_id', new TRequiredValidator);
        

        // add one row for each form field
        $table->addRowSet( new TLabel('ID'), $id );
        $table->addRowSet( $label_dt_consulta = new TLabel('Data Consulta'), $dt_consulta );
        $table->addRowSet($label_turno = new TLabel('Turno') , $turno );
        $table->addRowSet( $label_consultorio_id = new TLabel('Consultório'), array($consultorio_id, $consultorio_nome) );      
        $table->addRowSet( $label_estado_consulta_id = new TLabel('Estado Consulta'), $estado_consulta_id );
        $table->addRowSet( $label_system_user_id = new TLabel('Médico'), array($system_user_id,$system_user_name) );
        $table->addRowSet( $label_paciente_id = new TLabel('Paciente'), array ($paciente_id, $paciente_nome) );



        $this->form->setFields(array($turno,$paciente_nome,$consultorio_nome,$id,$dt_consulta,$consultorio_id,$estado_consulta_id,$system_user_id,$system_user_name,$paciente_id));


        // create the form actions
        $save_button = TButton::create('save', array($this, 'onSave'), _t('Save'), 'ico_save.png');
        $new_button  = TButton::create('new',  array($this, 'onEdit'), _t('New'),  'ico_new.png');
        
        $this->form->addField($save_button);
        $this->form->addField($new_button);
        
        $buttons_box = new THBox;
        $buttons_box->add($save_button);
        $buttons_box->add($new_button);
        
        // add a row for the form action
        $row = $table->addRow();
        $row->class = 'tformaction'; // CSS class
        $row->addCell($buttons_box)->colspan = 2;
        
        parent::add($this->form);
    }
    

    /**
     * method onSave()
     * Executed whenever the user clicks at the save button
     */
    function onSave()
    {
        if( $this->validateDados() )
        {
            try
            {
                TTransaction::open('db_consultas'); // open a transaction
                
                // get the form data into an active record Consulta
                $object = $this->form->getData('Consulta');
                $this->form->validate(); // form validation
                $object->store(); // stores the object
                $this->form->setData($object); // keep form data
                TTransaction::close(); // close the transaction
                
                // shows the success message
                new TMessage('info', TAdiantiCoreTranslator::translate('Record saved'));
            }
            catch (Exception $e) // in case of exception
            {
                new TMessage('error', '<b>Error</b> ' . $e->getMessage()); // shows the exception error message
                $this->form->setData( $this->form->getData() ); // keep form data
                TTransaction::rollback(); // undo all pending operations
            }
        }
        else
        {
            $this->form->setData( $this->form->getData() ); 
            new TMessage( 'error' , 'Conflito de dados' );
        }
    }
    
    /**
     * method onEdit()
     * Executed whenever the user clicks at the edit button da datagrid
     */
    function onEdit($param)
    {
        try
        {
            if (isset($param['key']))
            {
                $key=$param['key'];  // get the parameter $key
                TTransaction::open('db_consultas'); // open a transaction
                $object = new Consulta($key); // instantiates the Active Record
                $this->form->setData($object); // fill the form
                TTransaction::close(); // close the transaction
            }
            else
            {
                $this->form->clear();
                $dados = new stdClass;
                $dados->turno = 'M';
                $this->form->setData($dados);
            }
        }
        catch (Exception $e) // in case of exception
        {
            new TMessage('error', '<b>Error</b> ' . $e->getMessage()); // shows the exception error message
            TTransaction::rollback(); // undo all pending operations
        }
    }
    public function validateDados()
    {
        $dados = $this->form->getData( 'SystemUser' );        
        
            if (date('Y-m-d') > $dados->dt_consulta )
            {
                return false;
            }
            else
            {
                TTransaction::open('db_consultas');
                
                $repositorio = new TRepository( 'Consulta' );
                
                $repositorio = $repositorio->load();       
                        
                foreach ( $repositorio as $row )
                {
                  if( $row->dt_consulta == $dados->dt_consulta)
                  {
                      if( $row->turno == $dados->turno )
                      {
                          if( $row->system_user_id == $dados->system_user_id )
                          {   
                              if ( $row->estado_consulta_id == 1 )
                              {
                                  if( $row->id != $dados->id )
                                  {                   
                                      TTransaction::close();
                                      return false;
                                  }
                              }
                          }
                      }
                  }   
                }                              
                TTransaction::close();
        
        }
        return true;
    }
}
