<?php
/**
 * ConcluirAvisosForm Registration
 * @author  <your name here>
 */
class ConcluirAvisosForm extends TStandardForm
{
    protected $form; // form
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');              // defines the database
        parent::setActiveRecord('Avisos');     // defines the active record
        
        // creates the form
        $this->form = new TQuickForm('form_Concluir_Avisos');
        $this->form->class = 'tform'; // CSS class
        $this->form->style = 'width: 100%';
        
        // define the form title
        $this->form->setFormTitle('Aviso');
        


        // create the form fields
        $id                             = new TEntry('id');
        $nome                           = new TEntry('nome');
        $mensagem                       = new TText('mensagem');
        $dt_aviso                       = new TEntry('dt_aviso');
        $estado                         = new THidden('estado');


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Nome', $nome,  500);
        $this->form->addQuickField('Mensagem', $mensagem,  500);
        $this->form->addQuickField('Data Aviso', $dt_aviso,  500);
        $this->form->addQuickField('Estado', $estado,  500);


        $mensagem->setSize(500, 200);
        
        //setEditable
        $id->setEditable( FALSE );
        $nome->setEditable( FALSE );                       
        $mensagem->setEditable( FALSE );                      
        $dt_aviso->setEditable( FALSE );              
        $estado->setEditable( FALSE );  
               
        
        // create the form actions
        $this->form->addQuickAction('Concluir', new TAction(array($this, 'onSave')), 'ico_ok.png');
        $this->form->addQuickAction('Listar', new TAction(array($this, 'onList')), 'ico_datagrid.png');
        
        // add the form to the page
        parent::add($this->form);
    }
    
    public function onSave()
    {
        try
        {
            TTransaction::open('db_consultas');
            
            $dados = $this->form->getData();
            
            $aviso = new Avisos( $dados->id );
            
            $aviso->estado = 'C';
            
            $aviso->store();
                
            TTransaction::close();
            
            $this->onList();           
        }
        catch( Exception $e )
        {
            new TMessage( 'error' , $e->getMessage() );
        }
    }

    public function onList()
    {
        TApplication::gotoPage('AvisosList');
    }
   
}
















