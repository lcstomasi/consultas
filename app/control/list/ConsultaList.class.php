<?php
/**
 * ConsultaList Listing
 * @author  <your name here>
 */
class ConsultaList extends TStandardList
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    
    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');            // defines the database
        parent::setActiveRecord('Consulta');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order
        parent::addFilterField('id', '='); // add a filter field
        parent::addFilterField('system_user_id', '='); // add a filter field
        
        // creates the form, with a table inside
        $this->form = new TQuickForm('form_search_Consulta');
        $this->form->class = 'tform'; // CSS class
        $this->form->setFormTitle('Consulta');
        $this->form->style = 'width:100%';

        // create the form fields
        $id                             = new TEntry('id');
        $system_user_id                 = new TEntry('system_user_id');


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Medico', $system_user_id,  500);



        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('Consulta_filter_data') );
        
        // add the search form actions
        $this->form->addQuickAction(_t('Find'), new TAction(array($this, 'onSearch')), 'ico_find.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array('ConsultaForm', 'onEdit')), 'ico_new.png');
        
        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->setHeight(320);
        $this->datagrid->width = '100%';

        // creates the datagrid columns
        $id = $this->datagrid->addQuickColumn('ID', 'id', 'right', 100);
        $dt_consulta = $this->datagrid->addQuickColumn('Data Consulta', 'dt_consulta', 'right', 100, new TAction(array($this, 'onReload')), array('order', 'dt_consulta'));
        $turno = $this->datagrid->addQuickColumn('Turno', 'turno', 'right', 50);
        $consultorio_id = $this->datagrid->addQuickColumn('Consultório', 'consultorio_id', 'right', 100);
        $estado_consulta_id = $this->datagrid->addQuickColumn('Estado', 'estado_consulta_id', 'right', 100);
        $system_user_id = $this->datagrid->addQuickColumn('Médico', 'system_user_id', 'right', 100);
        $paciente_id = $this->datagrid->addQuickColumn('Paciente', 'paciente_id', 'right', 100);
        
        
        // Transformer
        $estado_consulta_id->setTransformer( array( $this, 'onEstadoConsulta' ) );
        $system_user_id->setTransformer( array( $this, 'onMedico') );
        $paciente_id->setTransformer( array( $this, 'onPaciente' ) );
        
        
        // create the datagrid actions
        $edit_action   = new TDataGridAction(array('ConsultaForm', 'onEdit'));
        $delete_action = new TDataGridAction(array($this, 'onDelete'));
        
        // add the actions to the datagrid
        $this->datagrid->addQuickAction(('Editar'), $edit_action, 'id', 'ico_edit.png');
        $this->datagrid->addQuickAction(('Cancelar'), $delete_action, 'id', 'ico_delete.png');
        $this->datagrid->width = '100%';
        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        // create the page container
        //$container = TVBox::pack( $this->form, $this->datagrid, $this->pageNavigation);
        $container =  new TElement('div');
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);
        parent::add($container);
    }
    
    public function onEstadoConsulta( $estado_consulta_id )
    {
        TTransaction::open('db_consultas');
        
        $estado = new EstadoConsulta ($estado_consulta_id );   
        
        TTransaction::close();
        
        return $estado->descricao;
    }
    
    public function onMedico ( $system_user_id )
    {
        TTransaction::open('permission');
        
        $medico = new SystemUser($system_user_id);   
        
        TTransaction::close();
        
        return $medico->name;
    }
    
    public function onPaciente( $paciente_id )
    {
        TTransaction::open('db_consultas');
        
        $paciente = new Paciente($paciente_id);   
        
        TTransaction::close();
        
        return $paciente->nome;        
    }
    
   public function onDelete( $param )
   {
       $id = $param[ 'key' ];
       
       TTransaction::open( 'db_consultas' );
       
           $consulta = new Consulta( $id );
           
           $consulta->estado_consulta_id = 2;
           
           $consulta->store();
              
       TTransaction::close();
      
       parent::onReload();
   } 
    
}
