<?php
/**
 * AvisosList Listing
 * @author  <your name here>
 */
class AvisosList extends TStandardList
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    
    /**
     * Class constructor
     * Creates the page, the form and the listing
     */
    public function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('db_consultas');            // defines the database
        parent::setActiveRecord('Avisos');   // defines the active record
        parent::setDefaultOrder('id', 'asc');         // defines the default order
        parent::addFilterField('id', '='); // add a filter field
        parent::addFilterField('nome', 'like'); // add a filter field
        
        // creates the form, with a table inside
        $this->form = new TQuickForm('form_search_Avisos');
        $this->form->class = 'tform'; // CSS class
        $this->form->setFormTitle('Avisos');
        $this->form->style = 'width:100%';
        

        // create the form fields
        $id                             = new TEntry('id');
        $nome                           = new TEntry('nome');


        // add the fields
        $this->form->addQuickField('ID', $id,  500);
        $this->form->addQuickField('Nome', $nome,  500);



        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('Avisos_filter_data') );
        
        // add the search form actions
        $this->form->addQuickAction(_t('Find'), new TAction(array($this, 'onSearch')), 'ico_find.png');
        $this->form->addQuickAction(_t('New'),  new TAction(array('AvisosForm', 'onEdit')), 'ico_new.png');
        
        // creates a DataGrid
        $this->datagrid = new TQuickGrid;
        $this->datagrid->setHeight(320);
        $this->datagrid->width = '100%';

        // creates the datagrid columns
        $id = $this->datagrid->addQuickColumn('ID', 'id', 'right', 50);
        $nome = $this->datagrid->addQuickColumn('Nome', 'nome', 'left', 200, new TAction(array($this, 'onReload')), array('order', 'nome'));
        $mensagem = $this->datagrid->addQuickColumn('Mensagem', 'mensagem', 'left', 200);
        $dt_aviso = $this->datagrid->addQuickColumn('Data', 'dt_aviso', 'right', 100, new TAction(array($this, 'onReload')), array('order', 'dt_aviso'));
        $estado = $this->datagrid->addQuickColumn('Estado', 'estado', 'right', 50);

        
        // create the datagrid actions
        $edit_action   = new TDataGridAction(array('ConcluirAvisosForm', 'onEdit'));
        
        // add the actions to the datagrid
        $this->datagrid->addQuickAction(('Edit'), $edit_action, 'id', 'ico_edit.png');
        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        // create the page container
        //$container = TVBox::pack( $this->form, $this->datagrid, $this->pageNavigation);
        $container =  new TElement('div');
        $container->add($this->form);
        $container->add($this->datagrid);
        $container->add($this->pageNavigation);
        parent::add($container);
    }    
}
