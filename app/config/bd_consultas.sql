--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: avisos; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE avisos (
    id integer NOT NULL,
    nome character varying(100),
    mensagem text,
    dt_aviso date,
    estado character(1)
);


ALTER TABLE avisos OWNER TO lucas_tomasi;

--
-- Name: avisos_id_seq; Type: SEQUENCE; Schema: public; Owner: lucas_tomasi
--

CREATE SEQUENCE avisos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE avisos_id_seq OWNER TO lucas_tomasi;

--
-- Name: avisos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lucas_tomasi
--

ALTER SEQUENCE avisos_id_seq OWNED BY avisos.id;


--
-- Name: consulta; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE consulta (
    id integer NOT NULL,
    dt_consulta date,
    diagnostico text,
    consultorio_id integer,
    estado_consulta_id integer,
    system_user_id integer,
    paciente_id integer,
    system_user_name character varying(100),
    turno character(1)
);


ALTER TABLE consulta OWNER TO lucas_tomasi;

--
-- Name: consulta_id_seq; Type: SEQUENCE; Schema: public; Owner: lucas_tomasi
--

CREATE SEQUENCE consulta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consulta_id_seq OWNER TO lucas_tomasi;

--
-- Name: consulta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lucas_tomasi
--

ALTER SEQUENCE consulta_id_seq OWNED BY consulta.id;


--
-- Name: consulta_medicamento; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE consulta_medicamento (
    id integer NOT NULL,
    consulta_id integer,
    medicamento_id integer
);


ALTER TABLE consulta_medicamento OWNER TO lucas_tomasi;

--
-- Name: consulta_medicamento_id_seq; Type: SEQUENCE; Schema: public; Owner: lucas_tomasi
--

CREATE SEQUENCE consulta_medicamento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consulta_medicamento_id_seq OWNER TO lucas_tomasi;

--
-- Name: consulta_medicamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lucas_tomasi
--

ALTER SEQUENCE consulta_medicamento_id_seq OWNED BY consulta_medicamento.id;


--
-- Name: consultorio; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE consultorio (
    id integer NOT NULL,
    sala integer,
    predio integer,
    consulta_id integer
);


ALTER TABLE consultorio OWNER TO lucas_tomasi;

--
-- Name: consultorio_id_seq; Type: SEQUENCE; Schema: public; Owner: lucas_tomasi
--

CREATE SEQUENCE consultorio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultorio_id_seq OWNER TO lucas_tomasi;

--
-- Name: consultorio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lucas_tomasi
--

ALTER SEQUENCE consultorio_id_seq OWNED BY consultorio.id;


--
-- Name: estado_consulta; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE estado_consulta (
    id integer NOT NULL,
    descricao character varying(100)
);


ALTER TABLE estado_consulta OWNER TO lucas_tomasi;

--
-- Name: estado_consulta_id_seq; Type: SEQUENCE; Schema: public; Owner: lucas_tomasi
--

CREATE SEQUENCE estado_consulta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estado_consulta_id_seq OWNER TO lucas_tomasi;

--
-- Name: estado_consulta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lucas_tomasi
--

ALTER SEQUENCE estado_consulta_id_seq OWNED BY estado_consulta.id;


--
-- Name: medicamento; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE medicamento (
    id integer NOT NULL,
    nome character varying(100),
    dt_venc date,
    dt_fab date
);


ALTER TABLE medicamento OWNER TO lucas_tomasi;

--
-- Name: medicamento_id_seq; Type: SEQUENCE; Schema: public; Owner: lucas_tomasi
--

CREATE SEQUENCE medicamento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE medicamento_id_seq OWNER TO lucas_tomasi;

--
-- Name: medicamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lucas_tomasi
--

ALTER SEQUENCE medicamento_id_seq OWNED BY medicamento.id;


--
-- Name: paciente; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE paciente (
    id integer NOT NULL,
    nome character varying(100),
    telefone character(13)
);


ALTER TABLE paciente OWNER TO lucas_tomasi;

--
-- Name: paciente_id_seq; Type: SEQUENCE; Schema: public; Owner: lucas_tomasi
--

CREATE SEQUENCE paciente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE paciente_id_seq OWNER TO lucas_tomasi;

--
-- Name: paciente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lucas_tomasi
--

ALTER SEQUENCE paciente_id_seq OWNED BY paciente.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY avisos ALTER COLUMN id SET DEFAULT nextval('avisos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consulta ALTER COLUMN id SET DEFAULT nextval('consulta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consulta_medicamento ALTER COLUMN id SET DEFAULT nextval('consulta_medicamento_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consultorio ALTER COLUMN id SET DEFAULT nextval('consultorio_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY estado_consulta ALTER COLUMN id SET DEFAULT nextval('estado_consulta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY medicamento ALTER COLUMN id SET DEFAULT nextval('medicamento_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY paciente ALTER COLUMN id SET DEFAULT nextval('paciente_id_seq'::regclass);


--
-- Data for Name: avisos; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO avisos VALUES (1, 'Médico', 'cancelar consulta taal', '2015-07-18', 'C');


--
-- Name: avisos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lucas_tomasi
--

SELECT pg_catalog.setval('avisos_id_seq', 1, true);


--
-- Data for Name: consulta; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO consulta VALUES (1, '2015-07-21', 'Paciente com dores de cabeça', 1, 3, 3, 1, 'Médico', 'M');


--
-- Name: consulta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lucas_tomasi
--

SELECT pg_catalog.setval('consulta_id_seq', 1, true);


--
-- Data for Name: consulta_medicamento; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO consulta_medicamento VALUES (1, 1, 1);
INSERT INTO consulta_medicamento VALUES (2, 1, 2);


--
-- Name: consulta_medicamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lucas_tomasi
--

SELECT pg_catalog.setval('consulta_medicamento_id_seq', 2, true);


--
-- Data for Name: consultorio; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO consultorio VALUES (1, 322, 5, NULL);


--
-- Name: consultorio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lucas_tomasi
--

SELECT pg_catalog.setval('consultorio_id_seq', 1, true);


--
-- Data for Name: estado_consulta; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO estado_consulta VALUES (1, 'Agendada');
INSERT INTO estado_consulta VALUES (2, 'Cancelada');
INSERT INTO estado_consulta VALUES (3, 'Realizada');


--
-- Name: estado_consulta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lucas_tomasi
--

SELECT pg_catalog.setval('estado_consulta_id_seq', 3, true);


--
-- Data for Name: medicamento; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO medicamento VALUES (1, 'Paracetamol 750', '2015-11-28', '2015-07-01');
INSERT INTO medicamento VALUES (2, 'Paracetamol 500', '2015-07-02', '2015-07-22');


--
-- Name: medicamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lucas_tomasi
--

SELECT pg_catalog.setval('medicamento_id_seq', 2, true);


--
-- Data for Name: paciente; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO paciente VALUES (1, 'Lucas Tomasi', '(55)9399-9921');


--
-- Name: paciente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lucas_tomasi
--

SELECT pg_catalog.setval('paciente_id_seq', 1, true);


--
-- Name: consulta_medicamento_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY consulta_medicamento
    ADD CONSTRAINT consulta_medicamento_pkey PRIMARY KEY (id);


--
-- Name: consulta_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY consulta
    ADD CONSTRAINT consulta_pkey PRIMARY KEY (id);


--
-- Name: consultorio_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY consultorio
    ADD CONSTRAINT consultorio_pkey PRIMARY KEY (id);


--
-- Name: estado_consulta_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY estado_consulta
    ADD CONSTRAINT estado_consulta_pkey PRIMARY KEY (id);


--
-- Name: medicamento_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY medicamento
    ADD CONSTRAINT medicamento_pkey PRIMARY KEY (id);


--
-- Name: paciente_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY paciente
    ADD CONSTRAINT paciente_pkey PRIMARY KEY (id);


--
-- Name: consulta_consultorio_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consulta
    ADD CONSTRAINT consulta_consultorio_id_fk FOREIGN KEY (consultorio_id) REFERENCES consultorio(id);


--
-- Name: consulta_estado_consulta_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consulta
    ADD CONSTRAINT consulta_estado_consulta_id_fk FOREIGN KEY (estado_consulta_id) REFERENCES estado_consulta(id);


--
-- Name: consulta_medicamento_consulta_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consulta_medicamento
    ADD CONSTRAINT consulta_medicamento_consulta_id_fk FOREIGN KEY (consulta_id) REFERENCES consulta(id);


--
-- Name: consulta_paciente_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consulta
    ADD CONSTRAINT consulta_paciente_id_fk FOREIGN KEY (paciente_id) REFERENCES paciente(id);


--
-- Name: consultorio_consulta_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY consultorio
    ADD CONSTRAINT consultorio_consulta_id_fk FOREIGN KEY (consulta_id) REFERENCES consulta(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
