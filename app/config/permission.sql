--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: system_group; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE system_group (
    id integer NOT NULL,
    name character varying(100)
);


ALTER TABLE system_group OWNER TO lucas_tomasi;

--
-- Name: system_group_program; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE system_group_program (
    id integer NOT NULL,
    system_group_id integer,
    system_program_id integer
);


ALTER TABLE system_group_program OWNER TO lucas_tomasi;

--
-- Name: system_program; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE system_program (
    id integer NOT NULL,
    name character varying(100),
    controller character varying(100)
);


ALTER TABLE system_program OWNER TO lucas_tomasi;

--
-- Name: system_user; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE system_user (
    id integer NOT NULL,
    name character varying(100),
    login character varying(100),
    password character varying(100),
    email character varying(100),
    frontpage_id integer
);


ALTER TABLE system_user OWNER TO lucas_tomasi;

--
-- Name: system_user_group; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE system_user_group (
    id integer NOT NULL,
    system_user_id integer,
    system_group_id integer
);


ALTER TABLE system_user_group OWNER TO lucas_tomasi;

--
-- Name: system_user_program; Type: TABLE; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

CREATE TABLE system_user_program (
    id integer NOT NULL,
    system_user_id integer,
    system_program_id integer
);


ALTER TABLE system_user_program OWNER TO lucas_tomasi;

--
-- Data for Name: system_group; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO system_group VALUES (1, 'ADMIN');
INSERT INTO system_group VALUES (3, 'Médicos');
INSERT INTO system_group VALUES (2, 'Secretárias');


--
-- Data for Name: system_group_program; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO system_group_program VALUES (1, 1, 1);
INSERT INTO system_group_program VALUES (2, 1, 2);
INSERT INTO system_group_program VALUES (3, 1, 3);
INSERT INTO system_group_program VALUES (4, 1, 4);
INSERT INTO system_group_program VALUES (5, 1, 5);
INSERT INTO system_group_program VALUES (6, 1, 6);
INSERT INTO system_group_program VALUES (7, 2, 8);
INSERT INTO system_group_program VALUES (8, 2, 9);
INSERT INTO system_group_program VALUES (9, 2, 12);
INSERT INTO system_group_program VALUES (10, 2, 13);
INSERT INTO system_group_program VALUES (11, 2, 15);
INSERT INTO system_group_program VALUES (12, 2, 16);
INSERT INTO system_group_program VALUES (13, 2, 17);
INSERT INTO system_group_program VALUES (14, 2, 18);
INSERT INTO system_group_program VALUES (15, 2, 19);
INSERT INTO system_group_program VALUES (16, 2, 21);
INSERT INTO system_group_program VALUES (17, 2, 22);
INSERT INTO system_group_program VALUES (18, 2, 23);
INSERT INTO system_group_program VALUES (19, 2, 24);
INSERT INTO system_group_program VALUES (20, 2, 25);
INSERT INTO system_group_program VALUES (21, 2, 26);
INSERT INTO system_group_program VALUES (22, 2, 27);
INSERT INTO system_group_program VALUES (23, 2, 28);
INSERT INTO system_group_program VALUES (24, 2, 29);
INSERT INTO system_group_program VALUES (25, 2, 30);
INSERT INTO system_group_program VALUES (26, 2, 31);
INSERT INTO system_group_program VALUES (27, 2, 33);


--
-- Data for Name: system_program; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO system_program VALUES (1, 'System Group Form', 'SystemGroupForm');
INSERT INTO system_program VALUES (2, 'System Group List', 'SystemGroupList');
INSERT INTO system_program VALUES (3, 'System Program Form', 'SystemProgramForm');
INSERT INTO system_program VALUES (4, 'System Program List', 'SystemProgramList');
INSERT INTO system_program VALUES (5, 'System User Form', 'SystemUserForm');
INSERT INTO system_program VALUES (6, 'System User List', 'SystemUserList');
INSERT INTO system_program VALUES (7, 'Common Page', 'CommonPage');
INSERT INTO system_program VALUES (8, 'ConsultorioFormList', 'ConsultorioFormList');
INSERT INTO system_program VALUES (9, 'EstadoConsultaFormList', 'EstadoConsultaFormList');
INSERT INTO system_program VALUES (10, 'Atendimento', 'Atendimento');
INSERT INTO system_program VALUES (11, 'AvisosForm', 'AvisosForm');
INSERT INTO system_program VALUES (12, 'ConcluirAvisosForm', 'ConcluirAvisosForm');
INSERT INTO system_program VALUES (13, 'ConsultaForm', 'ConsultaForm');
INSERT INTO system_program VALUES (14, 'ConsultaMedicoForm', 'ConsultaMedicoForm');
INSERT INTO system_program VALUES (15, 'MedicamentoForm', 'MedicamentoForm');
INSERT INTO system_program VALUES (16, 'MedicoForm', 'MedicoForm');
INSERT INTO system_program VALUES (17, 'PacienteForm', 'PacienteForm');
INSERT INTO system_program VALUES (18, 'AvisosList', 'AvisosList');
INSERT INTO system_program VALUES (19, 'ConsultaList', 'ConsultaList');
INSERT INTO system_program VALUES (20, 'ConsultasMedicoList', 'ConsultasMedicoList');
INSERT INTO system_program VALUES (21, 'EstadoConsultaList', 'EstadoConsultaList');
INSERT INTO system_program VALUES (22, 'MedicamentoList', 'MedicamentoList');
INSERT INTO system_program VALUES (23, 'MedicoList', 'MedicoList');
INSERT INTO system_program VALUES (24, 'PacienteList', 'PacienteList');
INSERT INTO system_program VALUES (25, 'DocumentoConsultaForm', 'DocumentoConsultaForm');
INSERT INTO system_program VALUES (26, 'GraficoConsultasPorDataForm', 'GraficoConsultasPorDataForm');
INSERT INTO system_program VALUES (27, 'GraficoConsultasPorEstado', 'GraficoConsultasPorEstado');
INSERT INTO system_program VALUES (28, 'RelatorioConsultaComQuebrasForm', 'RelatorioConsultaComQuebrasForm');
INSERT INTO system_program VALUES (29, 'RelatorioConsultaForm', 'RelatorioConsultaForm');
INSERT INTO system_program VALUES (30, 'RelatorioConsultaQuebraDiaSemanaForm', 'RelatorioConsultaQuebraDiaSemanaForm');
INSERT INTO system_program VALUES (31, 'RelatorioConsultaQuebraEstadoForm', 'RelatorioConsultaQuebraEstadoForm');
INSERT INTO system_program VALUES (32, 'Calendario', 'Calendario');
INSERT INTO system_program VALUES (33, 'CalendarioSec', 'CalendarioSec');


--
-- Data for Name: system_user; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO system_user VALUES (1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.net', 6);
INSERT INTO system_user VALUES (2, 'User', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user@user.net', 7);
INSERT INTO system_user VALUES (4, 'Secretária', 'sec', '74459ca3cf85a81df90da95ff6e7a207', 'secretaria@univates.com', 33);
INSERT INTO system_user VALUES (3, 'Médico', 'medico', '652044ac6e008761b3e6141748a99880', 'medico@univates.com', 32);


--
-- Data for Name: system_user_group; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO system_user_group VALUES (1, 1, 1);
INSERT INTO system_user_group VALUES (7, 4, 2);
INSERT INTO system_user_group VALUES (8, 3, 3);


--
-- Data for Name: system_user_program; Type: TABLE DATA; Schema: public; Owner: lucas_tomasi
--

INSERT INTO system_user_program VALUES (1, 2, 7);
INSERT INTO system_user_program VALUES (2, 3, 11);
INSERT INTO system_user_program VALUES (3, 3, 14);
INSERT INTO system_user_program VALUES (4, 3, 20);
INSERT INTO system_user_program VALUES (5, 3, 22);
INSERT INTO system_user_program VALUES (6, 3, 32);


--
-- Name: system_group_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY system_group
    ADD CONSTRAINT system_group_pkey PRIMARY KEY (id);


--
-- Name: system_group_program_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY system_group_program
    ADD CONSTRAINT system_group_program_pkey PRIMARY KEY (id);


--
-- Name: system_program_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY system_program
    ADD CONSTRAINT system_program_pkey PRIMARY KEY (id);


--
-- Name: system_user_group_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY system_user_group
    ADD CONSTRAINT system_user_group_pkey PRIMARY KEY (id);


--
-- Name: system_user_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY system_user
    ADD CONSTRAINT system_user_pkey PRIMARY KEY (id);


--
-- Name: system_user_program_pkey; Type: CONSTRAINT; Schema: public; Owner: lucas_tomasi; Tablespace: 
--

ALTER TABLE ONLY system_user_program
    ADD CONSTRAINT system_user_program_pkey PRIMARY KEY (id);


--
-- Name: system_group_program_system_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY system_group_program
    ADD CONSTRAINT system_group_program_system_group_id_fkey FOREIGN KEY (system_group_id) REFERENCES system_group(id);


--
-- Name: system_group_program_system_program_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY system_group_program
    ADD CONSTRAINT system_group_program_system_program_id_fkey FOREIGN KEY (system_program_id) REFERENCES system_program(id);


--
-- Name: system_user_frontpage_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY system_user
    ADD CONSTRAINT system_user_frontpage_id_fkey FOREIGN KEY (frontpage_id) REFERENCES system_program(id);


--
-- Name: system_user_group_system_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY system_user_group
    ADD CONSTRAINT system_user_group_system_group_id_fkey FOREIGN KEY (system_group_id) REFERENCES system_group(id);


--
-- Name: system_user_group_system_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY system_user_group
    ADD CONSTRAINT system_user_group_system_user_id_fkey FOREIGN KEY (system_user_id) REFERENCES system_user(id);


--
-- Name: system_user_program_system_program_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY system_user_program
    ADD CONSTRAINT system_user_program_system_program_id_fkey FOREIGN KEY (system_program_id) REFERENCES system_program(id);


--
-- Name: system_user_program_system_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lucas_tomasi
--

ALTER TABLE ONLY system_user_program
    ADD CONSTRAINT system_user_program_system_user_id_fkey FOREIGN KEY (system_user_id) REFERENCES system_user(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
